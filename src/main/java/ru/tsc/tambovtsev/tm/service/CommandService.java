package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.service.ICommandService;
import ru.tsc.tambovtsev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}